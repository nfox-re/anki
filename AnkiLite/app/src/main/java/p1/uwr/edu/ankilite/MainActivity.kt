package p1.uwr.edu.ankilite

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.Exception

class MainActivity : AppCompatActivity(), OnItemClickListener {

    lateinit var mFlashcardSet: HashMap<String, ArrayList<Pair<String, String>>>
    lateinit var jsonStr: String
    var mFlashcardSetNames: ArrayList<String> = ArrayList()
    var setNameList: ArrayList<FlashcardSetItem> = generateSetNameList()
    private val adapter = FlashcardSetAdapter(setNameList, this)
    private var fabInsert: FloatingActionButton? = null
    private var fabRemove: FloatingActionButton? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fabInsert = findViewById(R.id.Insert)
        fabRemove = findViewById(R.id.Remove)


        val mFlashcardView: RecyclerView = findViewById(R.id.flashcard_sets_listview)

        var sharedPref: SharedPreferences =
            getSharedPreferences("FLASHCARD_SET_JSON", Context.MODE_PRIVATE)

        jsonStr = sharedPref.getString("FLASHCARD_SET_JSON", null).toString()

        if (jsonStr == "null") {
            Log.d("jsonStr", "entering null area AAAAAAA")
            mFlashcardSet = hashMapOf(
                "Sample set" to arrayListOf(
                    Pair("Front 1", "Back 1"),
                    Pair("Front 2", "Back 2 "),
                    Pair("Front 3", "Back 3")
                )
            )
            mFlashcardSet["Set 2"] = arrayListOf(
                Pair("Front 1", "Back 1"),
                Pair("Front 2", "Back 2 "),
                Pair("Front 3", "Back 3")
            )
            mFlashcardSet["Set 3"] = arrayListOf(
                Pair("Front 1", "Back 1"),
                Pair("Front 2", "Back 2 "),
                Pair("Front 3", "Back 3")
            )

            jsonStr = Gson().toJson(mFlashcardSet)
        } else {
            Log.d("jsonStr", "we happy now maybe")
        }

        Log.d("||||YO, WTF||||", jsonStr)

        val mFlashCardTest: HashMap<String, ArrayList<Pair<String, String>>> =
            Gson().fromJson(
                jsonStr,
                object : TypeToken<HashMap<String, ArrayList<Pair<String, String>>>>() {}.type
            )

        mFlashcardSet = mFlashCardTest

        mFlashcardSetNames = ArrayList(mFlashcardSet.keys)

        for (el in mFlashcardSetNames) Log.d("xD", el)

        setNameList = generateSetNameList()

        flashcard_sets_listview.adapter = FlashcardSetAdapter(setNameList, this)
        flashcard_sets_listview.layoutManager = LinearLayoutManager(this)
        adapter.notifyDataSetChanged()
        flashcard_sets_listview.setHasFixedSize(true)
    }

    override fun onPause() {
        super.onPause()
        var sharedPref: SharedPreferences =
            getSharedPreferences("FLASHCARD_SET_JSON", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString("FLASHCARD_SET_JSON", jsonStr)
        editor.apply()

    }

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putSerializable(FLASHCARD_BANK, mFlashcardSet)
        outState.putSerializable(FLASHCARD_NAMES, mFlashcardSetNames)
        outState.putSerializable(FLASHCARD_NAMES_LIST, setNameList)
        outState.putString(JSON_STRING, jsonStr)
    }

    public override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        mFlashcardSet = savedInstanceState.getSerializable(FLASHCARD_BANK)
                as HashMap<String, ArrayList<Pair<String, String>>>

        mFlashcardSetNames = savedInstanceState.getSerializable(FLASHCARD_NAMES)
                as ArrayList<String>

        setNameList = savedInstanceState.getSerializable(FLASHCARD_NAMES_LIST)
                as ArrayList<FlashcardSetItem>

        jsonStr = savedInstanceState.getString(JSON_STRING).toString()

        flashcard_sets_listview.adapter = FlashcardSetAdapter(setNameList, this)
        adapter.notifyDataSetChanged()
    }

    fun newSetActivity(view: View) {
        val intent = Intent(this, NewSetActivity::class.java).apply {
            putExtra(FLASHCARD_BANK, mFlashcardSet)
            putExtra(FLASHCARD_NAMES, mFlashcardSetNames)
            putExtra(FLASHCARD_NAMES_LIST, setNameList)
            putExtra(JSON_STRING, jsonStr)
        }
        startActivityForResult(intent, 1)
    }

    fun removeSetActivity(view: View) {
        val intent = Intent(this, RemoveSetActivity::class.java).apply {
            putExtra(FLASHCARD_BANK, mFlashcardSet)
            putExtra(FLASHCARD_NAMES, mFlashcardSetNames)
            putExtra(FLASHCARD_NAMES_LIST, setNameList)
            putExtra(JSON_STRING, jsonStr)
        }
        startActivityForResult(intent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)
            if (data != null) {
                mFlashcardSet = data.getSerializableExtra(FLASHCARD_BANK)
                        as HashMap<String, ArrayList<Pair<String, String>>>

                mFlashcardSetNames = data.getSerializableExtra(FLASHCARD_NAMES)
                        as ArrayList<String>

                setNameList = data.getSerializableExtra(FLASHCARD_NAMES_LIST)
                        as ArrayList<FlashcardSetItem>

                jsonStr = data.getStringExtra(JSON_STRING).toString()

            }
        } catch (e: Exception) {
            Toast.makeText(this@MainActivity, e.toString(), Toast.LENGTH_SHORT).show()
        }

        flashcard_sets_listview.adapter = FlashcardSetAdapter(setNameList, this)
        adapter.notifyDataSetChanged()
    }

    fun reviewSetActivity(flashcardItem: FlashcardSetItem) {
        val intent = Intent(this, ReviewSetActivity::class.java).apply {
            putExtra(FLASHCARD_BANK, mFlashcardSet)
            putExtra(FLASHCARD_NAMES, mFlashcardSetNames)
            putExtra(FLASHCARD_NAMES_LIST, setNameList)
            putExtra(JSON_STRING, jsonStr)
            putExtra("curr name", flashcardItem.name)
            putExtra("curr set", flashcardItem)
        }
        startActivityForResult(intent, 1)
    }

    private fun generateSetNameList(): ArrayList<FlashcardSetItem> {

        val list = ArrayList<FlashcardSetItem>()

        for (name in mFlashcardSetNames) {
            val item =
                FlashcardSetItem(name, mFlashcardSet[name] as ArrayList<Pair<String, String>>)
            list += item
        }

        return list
    }

    companion object {
        const val FLASHCARD_BANK = "flashcard bank"
        const val FLASHCARD_NAMES = "flashcard names"
        const val FLASHCARD_NAMES_LIST = "flashcard names list"
        const val JSON_STRING = "json string"

    }

    override fun onItemClicked(flashcardItem: FlashcardSetItem) {
        flashcardItem.cards.forEach { (x, y) ->
            Log.d("onclicktest", "name = ${flashcardItem.name} 1 = $x, 2 = $y\n")
        }
        reviewSetActivity(flashcardItem)
    }

}
