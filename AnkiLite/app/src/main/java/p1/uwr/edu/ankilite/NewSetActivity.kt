package p1.uwr.edu.ankilite

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.gson.Gson

class NewSetActivity : AppCompatActivity() {

    private lateinit var mNewFlashcardSet: HashMap<String, ArrayList<Pair<String, String>>>
    private lateinit var mNewJsonStr: String
    private var mNewFlashcardSetNames: ArrayList<String> = ArrayList()
    private var mNewSetNameList: ArrayList<FlashcardSetItem> = generateSetNameList()
    private var mNewSetNameText: EditText? = null
    private var mNewSetNameStr: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_set)

        val recIntent = intent

        mNewFlashcardSet = recIntent.getSerializableExtra(MainActivity.FLASHCARD_BANK)
                as HashMap<String, ArrayList<Pair<String, String>>>

        mNewJsonStr = recIntent.getStringExtra(MainActivity.JSON_STRING)

        mNewFlashcardSetNames = recIntent.getSerializableExtra(MainActivity.FLASHCARD_NAMES)
                as ArrayList<String>

        mNewSetNameList = recIntent.getSerializableExtra(MainActivity.FLASHCARD_NAMES_LIST)
                as ArrayList<FlashcardSetItem>

        mNewSetNameText = findViewById(R.id.newSetNameEditText)
    }

    fun addNewSet(view: View?) {

        mNewSetNameStr = mNewSetNameText!!.text.toString()

        if (mNewSetNameStr!!.isNotEmpty()) {
            if (!mNewFlashcardSetNames.contains(mNewSetNameStr!!)) {
                mNewFlashcardSet[mNewSetNameStr!!] = arrayListOf()
                mNewFlashcardSetNames.add(mNewSetNameStr!!)
                mNewSetNameList = generateSetNameList()
                mNewJsonStr = Gson().toJson(mNewFlashcardSet)
                onBackPressed()
            } else {
                Toast.makeText(
                    this@NewSetActivity, "Set with this name already exists",
                    Toast.LENGTH_SHORT
                ).show()
            }
        } else {
            Toast.makeText(
                this@NewSetActivity, "Fill missing data",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("text field str", mNewSetNameText!!.text.toString())
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val resStr = savedInstanceState.getString("text field str")
        mNewSetNameText!!.setText(resStr)
    }

    override fun onBackPressed() {

        val returnIntent = Intent()
        returnIntent.putExtra(MainActivity.FLASHCARD_NAMES_LIST, mNewSetNameList)
        returnIntent.putExtra(MainActivity.FLASHCARD_NAMES, mNewFlashcardSetNames)
        returnIntent.putExtra(MainActivity.FLASHCARD_BANK, mNewFlashcardSet)
        returnIntent.putExtra(MainActivity.JSON_STRING, mNewJsonStr)
        setResult(1, returnIntent)
        finish()

        super.onBackPressed()
    }


    private fun generateSetNameList(): ArrayList<FlashcardSetItem> {

        val list = ArrayList<FlashcardSetItem>()

        for (name in mNewFlashcardSetNames) {
            val item =
                FlashcardSetItem(name, mNewFlashcardSet[name] as ArrayList<Pair<String, String>>)
            list += item
        }

        return list
    }

}
