package p1.uwr.edu.ankilite

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class NewCardActivity : AppCompatActivity() {

    private lateinit var mNewFlashcardSet: HashMap<String, ArrayList<Pair<String, String>>>
    private lateinit var mNewJsonStr: String
    private var mNewFlashcardSetNames: ArrayList<String> = ArrayList()
    private var mNewSetNameList: ArrayList<FlashcardSetItem> = generateSetNameList()

    private var mNewFrontEditText: EditText? = null
    private var mNewBackEditText: EditText? = null
    private var mNewFrontStr: String? = null
    private var mNewBackStr: String? = null
    private var mCurrentName: String? = null

    private var mCurrentSet: FlashcardSetItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_card)

        val recIntent = intent

        mNewFlashcardSet = recIntent.getSerializableExtra(MainActivity.FLASHCARD_BANK)
                as HashMap<String, ArrayList<Pair<String, String>>>

        mNewJsonStr = recIntent.getStringExtra(MainActivity.JSON_STRING)

        mNewFlashcardSetNames = recIntent.getSerializableExtra(MainActivity.FLASHCARD_NAMES)
                as ArrayList<String>

        mNewSetNameList = recIntent.getSerializableExtra(MainActivity.FLASHCARD_NAMES_LIST)
                as ArrayList<FlashcardSetItem>

        mCurrentName = recIntent.getStringExtra("curr name")

        mCurrentSet = recIntent.getSerializableExtra("curr set") as FlashcardSetItem?

        mNewBackEditText = findViewById(R.id.CardBackExitText)
        mNewFrontEditText = findViewById(R.id.CardFrontEditText)

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("new front text", mNewFrontEditText!!.text.toString())
        outState.putString("new back text", mNewBackEditText!!.text.toString())
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        mNewFrontStr = savedInstanceState.getString("new front text")
        mNewBackStr = savedInstanceState.getString("new back text")
        updateTextView()
    }

    private fun updateTextView() {
        mNewFrontEditText!!.setText(mNewFrontStr)
        mNewBackEditText!!.setText(mNewBackStr)
    }

    fun addCard(view: View?) {

        mNewBackStr = mNewBackEditText!!.text.toString()
        mNewFrontStr = mNewFrontEditText!!.text.toString()

        if (!mNewBackStr.isNullOrEmpty() && !mNewFrontStr.isNullOrEmpty()) {

            mNewFlashcardSet[mCurrentName]?.add(
                Pair(
                    mNewFrontStr,
                    mNewBackStr
                ) as Pair<String, String>
            )
            mNewSetNameList = generateSetNameList()
            mNewJsonStr = Gson().toJson(mNewFlashcardSet)
            mCurrentSet?.cards?.add(Pair(mNewFrontStr, mNewBackStr) as Pair<String, String>)

            onBackPressed()
        } else {
            Toast.makeText(
                this@NewCardActivity, "Fill missing data",
                Toast.LENGTH_SHORT
            ).show()
        }
    }


    override fun onBackPressed() {

        val returnIntent = Intent()
        returnIntent.putExtra(MainActivity.FLASHCARD_NAMES_LIST, mNewSetNameList)
        returnIntent.putExtra(MainActivity.FLASHCARD_NAMES, mNewFlashcardSetNames)
        returnIntent.putExtra(MainActivity.FLASHCARD_BANK, mNewFlashcardSet)
        returnIntent.putExtra(MainActivity.JSON_STRING, mNewJsonStr)
        returnIntent.putExtra("curr set", mCurrentSet)
        setResult(2, returnIntent)
        finish()

        super.onBackPressed()
    }


    private fun generateSetNameList(): ArrayList<FlashcardSetItem> {

        val list = ArrayList<FlashcardSetItem>()

        for (name in mNewFlashcardSetNames) {
            val item =
                FlashcardSetItem(name, mNewFlashcardSet[name] as ArrayList<Pair<String, String>>)
            list += item
        }
        return list
    }
}
