package p1.uwr.edu.ankilite

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import kotlin.properties.Delegates

class ReviewSetActivity : AppCompatActivity() {

    private lateinit var mNewFlashcardSet: HashMap<String, ArrayList<Pair<String, String>>>
    private lateinit var mNewJsonStr: String
    private var mNewFlashcardSetNames: ArrayList<String> = ArrayList()
    private var mNewSetNameList: ArrayList<FlashcardSetItem> = generateSetNameList()
    private var mCurrIndex by Delegates.notNull<Int>()
    private var mQuestionTextView: TextView? = null
    private var mQuestionCurrentText: String? = null
    private var mCurrentName: String? = null
    private var mCurrentSide: Int? = null

    private var mCurrentSet: FlashcardSetItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review_set)

        val recIntent = intent

        mCurrIndex = 0

        mNewFlashcardSet = recIntent.getSerializableExtra(MainActivity.FLASHCARD_BANK)
                as HashMap<String, ArrayList<Pair<String, String>>>

        mNewJsonStr = recIntent.getStringExtra(MainActivity.JSON_STRING)

        mNewFlashcardSetNames = recIntent.getSerializableExtra(MainActivity.FLASHCARD_NAMES)
                as ArrayList<String>

        mNewSetNameList = recIntent.getSerializableExtra(MainActivity.FLASHCARD_NAMES_LIST)
                as ArrayList<FlashcardSetItem>

        mQuestionTextView = findViewById(R.id.question_text_view)

        mCurrentSet = recIntent.getSerializableExtra("curr set") as FlashcardSetItem?

        mCurrentName = recIntent.getStringExtra("curr name")
        mCurrentSide = 1

        if (mCurrentSet?.cards?.size == 0)
            mQuestionTextView!!.setText("Add first card").toString()
        else {
            mQuestionCurrentText = mCurrentSet?.cards?.get(mCurrIndex)?.first
            mQuestionTextView?.setText(mQuestionCurrentText).toString()
        }
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("current idx", mCurrIndex)
        outState.putString("current text", mQuestionCurrentText)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        mCurrIndex = savedInstanceState.getInt("current idx")
        mQuestionCurrentText = savedInstanceState.getString("current text")
        updateTextView()
    }

    override fun onBackPressed() {

        val returnIntent = Intent()
        returnIntent.putExtra(MainActivity.FLASHCARD_NAMES_LIST, mNewSetNameList)
        returnIntent.putExtra(MainActivity.FLASHCARD_NAMES, mNewFlashcardSetNames)
        returnIntent.putExtra(MainActivity.FLASHCARD_BANK, mNewFlashcardSet)
        returnIntent.putExtra(MainActivity.JSON_STRING, mNewJsonStr)
        setResult(1, returnIntent)
        finish()

        super.onBackPressed()
    }

    override fun onPause() {
        super.onPause()
        var sharedPref: SharedPreferences =
            getSharedPreferences("FLASHCARD_SET_JSON", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        mNewJsonStr = Gson().toJson(mNewFlashcardSet)
        editor.putString("FLASHCARD_SET_JSON", mNewJsonStr)
        editor.apply()

    }

    fun flipCard(view: View?) {
        if (!mCurrentSet?.cards.isNullOrEmpty()) {
            if (mCurrentSide == 1) {
                mQuestionCurrentText = mCurrentSet?.cards?.get(mCurrIndex)?.second
                updateTextView()
                mCurrentSide = 2
            } else {
                mQuestionCurrentText = mCurrentSet?.cards?.get(mCurrIndex)?.first
                updateTextView()
                mCurrentSide = 1
            }
        } else {
            currentSetEmptyMessage()
        }
    }

    private fun currentSetEmptyMessage() {
        Toast.makeText(
            this@ReviewSetActivity,
            "No cards in this set. Consider adding some.",
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun updateTextView() {
        mQuestionTextView!!.setText(mQuestionCurrentText)
    }

    fun next(view: View?) {
        if (!mCurrentSet?.cards.isNullOrEmpty()) {
            mCurrIndex = ++mCurrIndex % (mCurrentSet?.cards?.size!!)
            Log.d("next wtf", "$mCurrIndex, ${mCurrentSet!!.cards.size}")
            mQuestionCurrentText = mCurrentSet!!.cards[mCurrIndex].first
            updateTextView()
            mCurrentSide = 1
        } else {
            currentSetEmptyMessage()
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun removeCard(view: View?) {
        if (!mCurrentSet?.cards.isNullOrEmpty()) {

            mCurrentSet?.cards?.removeAt(mCurrIndex)
            mNewFlashcardSet[mCurrentName]?.removeAt(mCurrIndex)

            mNewSetNameList = generateSetNameList()
            mNewJsonStr = Gson().toJson(mNewFlashcardSet)

            if (mCurrentSet?.cards?.size!! >= 1) {

                mCurrIndex = Math.floorMod(--mCurrIndex, (mCurrentSet?.cards?.size!!))
                mQuestionCurrentText = mCurrentSet!!.cards[mCurrIndex].first

            } else {

                mQuestionCurrentText = "Add first card"
                mCurrIndex = 0
            }

            updateTextView()

        } else {
            currentSetEmptyMessage()
        }
    }

    fun newCardActivity(view: View) {
        val intent = Intent(this, NewCardActivity::class.java).apply {
            putExtra(MainActivity.FLASHCARD_BANK, mNewFlashcardSet)
            putExtra(MainActivity.FLASHCARD_NAMES, mNewFlashcardSetNames)
            putExtra(MainActivity.FLASHCARD_NAMES_LIST, mNewSetNameList)
            putExtra(MainActivity.JSON_STRING, mNewJsonStr)
            putExtra("curr set", mCurrentSet)
            putExtra("curr name", mCurrentName)
        }
        startActivityForResult(intent, 2)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)
            if (data != null) {
                mNewFlashcardSet = data.getSerializableExtra(MainActivity.FLASHCARD_BANK)
                        as HashMap<String, ArrayList<Pair<String, String>>>

                mNewFlashcardSetNames = data.getSerializableExtra(MainActivity.FLASHCARD_NAMES)
                        as ArrayList<String>

                mNewSetNameList = data.getSerializableExtra(MainActivity.FLASHCARD_NAMES_LIST)
                        as ArrayList<FlashcardSetItem>

                mNewJsonStr = data.getStringExtra(MainActivity.JSON_STRING).toString()

                mCurrentSet = data.getSerializableExtra("curr set") as FlashcardSetItem?
                if (mNewFlashcardSet[mCurrentName]?.size == 1) {
                    mQuestionCurrentText = mCurrentSet?.cards?.get(mCurrIndex)?.first
                    updateTextView()
                }


            }
        } catch (e: Exception) {
            Toast.makeText(this@ReviewSetActivity, e.toString(), Toast.LENGTH_SHORT).show()
        }
    }


    @RequiresApi(Build.VERSION_CODES.N)
    fun prev(view: View?) {
        if (!mCurrentSet?.cards.isNullOrEmpty()) {
            mCurrIndex = Math.floorMod(--mCurrIndex, (mCurrentSet?.cards?.size!!))
            mQuestionCurrentText = mCurrentSet!!.cards[mCurrIndex].first
            updateTextView()
            mCurrentSide = 1
        } else {
            currentSetEmptyMessage()
        }

    }


    private fun generateSetNameList(): ArrayList<FlashcardSetItem> {

        val list = ArrayList<FlashcardSetItem>()

        for (name in mNewFlashcardSetNames) {
            val item =
                FlashcardSetItem(name, mNewFlashcardSet[name] as ArrayList<Pair<String, String>>)
            list += item
        }

        return list
    }
}
