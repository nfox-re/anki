package p1.uwr.edu.ankilite

import android.content.Intent
import android.content.ReceiverCallNotAllowedException
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.flashcard_set_item.view.*

class FlashcardSetAdapter(private val flashcardSetList: List<FlashcardSetItem>, val itemClickListener: OnItemClickListener) :
    RecyclerView.Adapter<FlashcardSetAdapter.FlashcardSetViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlashcardSetViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.flashcard_set_item,
            parent, false
        )
        return FlashcardSetViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: FlashcardSetViewHolder, position: Int) {
        val currentItem = flashcardSetList[position]
        holder.textView.text = currentItem.name
        holder.itemView.setOnClickListener {
            /*flashcardSetList[position].cards.forEach { (x, y) ->
                Log.d("onclicktest", "name = ${currentItem.name} 1 = $x, 2 = $y\n")
            }*/
            itemClickListener.onItemClicked(currentItem)

        }
    }

    override fun getItemCount(): Int {
        return flashcardSetList.size
    }

    class FlashcardSetViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textView: TextView = itemView.text_view1
    }
}

interface OnItemClickListener {
    fun onItemClicked(flashcardItem: FlashcardSetItem)
}