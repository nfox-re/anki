package p1.uwr.edu.ankilite

import java.io.Serializable

data class FlashcardSetItem(val name: String, var cards: ArrayList<Pair<String, String>>) :
    Serializable {
    var isSelected: Boolean = false
    var lastIndext: Int = 0
}