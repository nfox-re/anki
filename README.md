# Anki Helper Lite

Projekt zakłada stworzenie spersonalizowanej aplikacji mobilnej wspomagającej uczenie metodą spaced-repetition.

W skrócie:

aplikacja ma pozwalać na tworznie własnych zestawów 'flashcards', tudzież fiszek, oraz diagnozować poziom przyswojenia sobie wiedzy z danego zestawu. Ma też za zadanie pilnować oraz umożliwiać edycję/planowanie odstępów czasowych, w jakich miałby nastąpić kolejny 'trening' czy też powtórka.

Inspiracja w wydaniu mobilnym: https://www.ankiapp.com/
